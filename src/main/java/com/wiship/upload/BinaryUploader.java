package com.wiship.upload;

import com.wiship.PathFormat;
import com.wiship.define.BaseState;
import com.wiship.define.FileType;
import com.wiship.define.State;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class BinaryUploader {
    public BinaryUploader() {
    }

    public static final State save(MultipartFile upfile, Map<String, Object> conf) {
        if (upfile == null) {
            return new BaseState(false, 7);
        } else {
            try {
                String savePath = (String)conf.get("savePath");
                String originalFilename = upfile.getOriginalFilename();
                String suffix = FileType.getSuffixByFilename(originalFilename);
                originalFilename = originalFilename.substring(0, originalFilename.length() - suffix.length());
                savePath = savePath + suffix;
                long maxSize = (Long)conf.get("maxSize");
                if (!validType(suffix, (String[])conf.get("allowFiles"))) {
                    return new BaseState(false, 8);
                } else {
                    savePath = PathFormat.parse(savePath, originalFilename);
                    String physicalPath = (String)conf.get("rootPath") + savePath;
                    InputStream is = upfile.getInputStream();
                    State storageState = StorageManager.saveFileByInputStream(is, physicalPath, maxSize);
                    is.close();
                    if (storageState.isSuccess()) {
                        storageState.putInfo("url", PathFormat.format(savePath));
                        storageState.putInfo("type", suffix);
                        storageState.putInfo("original", originalFilename + suffix);
                    }

                    return storageState;
                }
            } catch (IOException var15) {
                return new BaseState(false, 4);
            }
        }
    }

    private static boolean validType(String type, String[] allowTypes) {
        List<String> list = Arrays.asList(allowTypes);
        return list.contains(type);
    }
}
