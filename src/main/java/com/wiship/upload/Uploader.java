package com.wiship.upload;

import com.wiship.define.State;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public class Uploader {
    private Map<String, Object> conf = null;
    private MultipartFile upfile = null;
    private String base64 = null;

    public Uploader(MultipartFile upfile, Map<String, Object> conf) {
        this.upfile = upfile;
        this.conf = conf;
    }

    public Uploader(String base64, Map<String, Object> conf) {
        this.base64 = base64;
        this.conf = conf;
    }

    public final State doExec() {
        State state = null;
        if (this.base64 != null) {
            state = Base64Uploader.save(this.base64, this.conf);
        } else {
            state = BinaryUploader.save(this.upfile, this.conf);
        }
        return state;
    }
}