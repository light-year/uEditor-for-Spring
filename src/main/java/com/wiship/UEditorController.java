package com.wiship;

import com.alibaba.fastjson.JSONObject;
import com.wiship.Properties.UEditorProperties;
import com.wiship.define.BaseState;
import com.wiship.hunter.FileManager;
import com.wiship.hunter.ImageHunter;
import com.wiship.upload.Uploader;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

public class UEditorController {
    protected UEditorProperties config;
    protected String rootPath;

    protected void setConfig(UEditorProperties config, String rootPath) {
        this.config = config;
        this.rootPath = rootPath;
    }

    public String config(String callback,
                         String action,
                         Object upfile,
                         String [] source,
                         Integer start) {
        if (action == null) {
            return new BaseState(false, 101).toJSONString();
        }
        if (callback == null) {
            return invoke(action, upfile, source, start);
        } else if (!callback.matches("^[a-zA-Z_]+[\\w0-9_]*$")) {
            return new BaseState(false, 401).toJSONString();
        } else {
            return callback + "(" + invoke(action,upfile, source, start) + ");";
        }
    }

    protected String invoke(String action,
                            Object upfile,
                            String [] list,
                            Integer start) {
        Map<String, Object> conf = null;
        switch (action) {
            case "config":
                return JSONObject.toJSONString(config);
            case "uploadimage":
            case "uploadscrawl":
            case "uploadvideo":
            case "uploadfile":
                conf = getConfig(config, action);
                if ("true".equals(conf.get("isBase64"))) {
                    String base64 = (String) upfile;
                    return (new Uploader(base64, conf)).doExec().toJSONString();
                } else {
                    MultipartFile multipartFile = (MultipartFile) upfile;
                    return (new Uploader(multipartFile, conf)).doExec().toJSONString();
                }
            case "catchimage":
                conf = getConfig(config, action);
                return (new ImageHunter(conf)).capture(list).toJSONString();
            case "listfile":
            case "listimage":
                conf = getConfig(config, action);
                return (new FileManager(conf)).listFile(start).toJSONString();
            default:
                return (new BaseState(false, 101)).toJSONString();
        }
    }

    protected Map<String, Object> getConfig(UEditorProperties config, String action) {
        Map<String, Object> conf = new HashMap();
        String savePath = null;
        switch(action) {
            case "uploadimage":
                conf.put("isBase64", "false");
                conf.put("maxSize", config.getImageMaxSize());
                conf.put("allowFiles", config.getImageAllowFiles());
                conf.put("fieldName", config.getImageFieldName());
                savePath = config.getImagePathFormat();
                break;
            case "uploadscrawl":
                conf.put("filename", "scrawl");
                conf.put("maxSize", config.getScrawlMaxSize());
                conf.put("fieldName", config.getScrawlFieldName());
                conf.put("isBase64", "true");
                savePath = config.getScrawlPathFormat();
                break;
            case "uploadvideo":
                conf.put("maxSize", config.getVideoMaxSize());
                conf.put("allowFiles", config.getVideoAllowFiles());
                conf.put("fieldName", config.getVideoFieldName());
                savePath = config.getVideoPathFormat();
                break;
            case "uploadfile":
                conf.put("isBase64", "false");
                conf.put("maxSize", config.getFileMaxSize());
                conf.put("allowFiles", config.getFileAllowFiles());
                conf.put("fieldName", config.getFileFieldName());
                savePath = config.getFilePathFormat();
                break;
            case "catchimage":
                conf.put("filename", "remote");
                conf.put("filter", config.getCatcherLocalDomain());
                conf.put("maxSize", config.getCatcherMaxSize());
                conf.put("allowFiles", config.getCatcherAllowFiles());
                conf.put("fieldName", config.getCatcherFieldName() + "[]");
                savePath = config.getCatcherPathFormat();
                break;
            case "listfile":
                conf.put("allowFiles", config.getFileManagerAllowFiles());
                conf.put("dir", config.getFileManagerListPath());
                conf.put("count", config.getFileManagerListSize());
                break;
            case "listimage":
                conf.put("allowFiles", config.getImageManagerAllowFiles());
                conf.put("dir", config.getImageManagerListPath());
                conf.put("count", config.getImageManagerListSize());
        }
        conf.put("savePath", savePath);
        conf.put("rootPath", rootPath); // todo
        return conf;
    }
}
