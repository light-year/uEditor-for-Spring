# uEditor for Spring

[English description](https://gitee.com/light-year/uEditor-for-Spring/tree/master/document)

### 项目介绍
百度uEditor适配spring

[uEditor官网](https://ueditor.baidu.com/website/index.html)

### 使用方法

##### 拷贝 ueditor.yml

将`dist/ueditor.yml`拷贝到你的项目的`resources/config/`目录下，
uEditor后端配置都可以在这里面进行

##### 拷贝 ueditor-for-spring-1.0.jar

将`dist/ueditor-for-spring-1.0.jar`拷贝到你的项目根目录的`lib/`目录下，
然后在你的pom.xml添加如下代码
```xml
<dependencies>
	<dependency>
			<groupId>commons-fileupload</groupId>
			<artifactId>commons-fileupload</artifactId>
			<version>1.3.3</version>
	</dependency>
	<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>2.6</version>
	</dependency>
	<dependency>
			<groupId>commons-codec</groupId>
			<artifactId>commons-codec</artifactId>
			<version>1.11</version>
	</dependency>
	<dependency>
			<groupId>com.alibaba</groupId>
			<artifactId>fastjson</artifactId>
			<version>1.2.51</version>
	</dependency>
	<dependency>
			<groupId>com.wiship</groupId>
			<artifactId>ueditor-for-spring</artifactId>
			<version>1.0</version>
			<scope>system</scope>
			<systemPath>${project.basedir}/lib/ueditor-for-spring-1.0.jar</systemPath>
	</dependency>
</dependencies>
```

##### 创建 yml 加载器
```java
import org.springframework.boot.env.YamlPropertySourceLoader;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.support.DefaultPropertySourceFactory;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.lang.Nullable;

import java.io.IOException;

public class YamlPropertySourceFactory extends DefaultPropertySourceFactory {
    public PropertySource<?> createPropertySource(@Nullable String name, EncodedResource resource) throws IOException {
        if (resource == null) {
            super.createPropertySource(name, resource);
        }
        return new YamlPropertySourceLoader().load(resource.getResource().getFilename(), resource.getResource()).get(0);
    }
}
```

##### 新建 UEditorProperties
```java
@Component
@ConfigurationProperties(prefix="ueditor")
@PropertySource(
       value = "classpath:config/ueditor.yml",
       factory = YamlPropertySourceFactory.class)
public class UEditorProperties extends com.wiship.Properties.UEditorProperties { 

}
```

##### 新建 UEditorBackendController
```java
import com.wiship.UEditorController;

@RestController
@RequestMapping("/backend/ueditor")
public class UEditorBackendController extends UEditorController {
    private static final Logger logger = LoggerFactory.getLogger(UEditorBackendController.class);

    @Autowired
    private UEditorProperties uEditorProperties;
    // 上传目录
    private String rootPath = "C:/project/";

    @RequestMapping("/config")
    @Override
    public String config(@RequestParam(required = false) String callback,
                         @RequestParam(required = false) String action,
                         @RequestParam(name = "upfile", required = false) Object upfile,
                         @RequestParam(defaultValue = "[]") String [] source,
                         @RequestParam(defaultValue = "0") Integer start) {
        super.setConfig(uEditorProperties, rootPath);
        return super.config(callback, action, upfile, source, start);
    }
}
```


### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)