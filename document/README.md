# uEditor for Spring

[中文说明](https://gitee.com/light-year/uEditor-for-Spring/tree/master/)

### Project introduction
Baidu's uEditor adaptation spring project

[uEditor official website](https://ueditor.baidu.com/website/index.html)

### Usage method

##### Copy ueditor.yml

Copy `dist/ueditor.yml` to the `resources/config/` directory of your project, 
UEditor backend configuration can be carried out in it.

##### Copy ueditor-for-spring-1.0.jar

Copy `dist/ueditor-for-spring-1.0.jar` to the `lib/` directory of your project root directory, 
Then add the following code to your pom.xml.
```xml
<dependencies>
	<dependency>
			<groupId>commons-fileupload</groupId>
			<artifactId>commons-fileupload</artifactId>
			<version>1.3.3</version>
	</dependency>
	<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>2.6</version>
	</dependency>
	<dependency>
			<groupId>commons-codec</groupId>
			<artifactId>commons-codec</artifactId>
			<version>1.11</version>
	</dependency>
	<dependency>
			<groupId>com.alibaba</groupId>
			<artifactId>fastjson</artifactId>
			<version>1.2.51</version>
	</dependency>
	<dependency>
			<groupId>com.wiship</groupId>
			<artifactId>ueditor-for-spring</artifactId>
			<version>1.0</version>
			<scope>system</scope>
			<systemPath>${project.basedir}/lib/ueditor-for-spring-1.0.jar</systemPath>
	</dependency>
</dependencies>
```

##### Create YML loader
```java
import org.springframework.boot.env.YamlPropertySourceLoader;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.support.DefaultPropertySourceFactory;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.lang.Nullable;

import java.io.IOException;

public class YamlPropertySourceFactory extends DefaultPropertySourceFactory {
    public PropertySource<?> createPropertySource(@Nullable String name, EncodedResource resource) throws IOException {
        if (resource == null) {
            super.createPropertySource(name, resource);
        }
        return new YamlPropertySourceLoader().load(resource.getResource().getFilename(), resource.getResource()).get(0);
    }
}
```

##### Create UEditorProperties
```java
@Component
@ConfigurationProperties(prefix="ueditor")
@PropertySource(
       value = "classpath:config/ueditor.yml",
       factory = YamlPropertySourceFactory.class)
public class UEditorProperties extends com.wiship.Properties.UEditorProperties { 

}
```

##### Create UEditorBackendController
```java
import com.wiship.UEditorController;

@RestController
@RequestMapping("/backend/ueditor")
public class UEditorBackendController extends UEditorController {
    private static final Logger logger = LoggerFactory.getLogger(UEditorBackendController.class);

    @Autowired
    private UEditorProperties uEditorProperties;
    // 上传目录
    private String rootPath = "C:/project/";

    @RequestMapping("/config")
    @Override
    public String config(@RequestParam(required = false) String callback,
                         @RequestParam(required = false) String action,
                         @RequestParam(name = "upfile", required = false) Object upfile,
                         @RequestParam(defaultValue = "[]") String [] source,
                         @RequestParam(defaultValue = "0") Integer start) {
        super.setConfig(uEditorProperties, rootPath);
        return super.config(callback, action, upfile, source, start);
    }
}
```
